
(function($) {

    var namespace = 'googlePlaces';

    $.googlePlaces = function(element, options) {

        var defaults = {
              placeId: 'ChIJN1t_tDeuEmsRUsoyG83frY4' // placeId provided by google api documentation
            , render: ['reviews']
            , min_rating: 0
            , max_rows: 0
        };

        var plugin = this;

        plugin.settings = {}

        var $element = $(element),
             element = element;

        plugin.init = function() {
          plugin.settings = $.extend({}, defaults, options);
          plugin.settings.schema = $.extend({}, defaults.schema, options.schema);
          $element.html("<div id='" + plugin.settings.map_plug_id + "'></div>"); // create a plug for google to load data into
          initialize_place(function(place){
            plugin.place_data = place;

            // Trigger event before render
            $element.trigger('beforeRender.' + namespace);

            // render specified sections
            if(plugin.settings.render.indexOf('reviews') > -1){
              renderReviews(plugin.place_data.reviews);
            }

            // Trigger event after render
            $element.trigger('afterRender.' + namespace);

          });
        }

        var initialize_place = function(c){
          var map = new google.maps.Map(document.getElementById(plugin.settings.map_plug_id));

          var request = {
            placeId: plugin.settings.placeId
          };

          var service = new google.maps.places.PlacesService(map);

          service.getDetails(request, function(place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
              c(place);
            }
          });
        }

        var sort_by_date = function(ray) {
          ray.sort(function(a, b){
            var keyA = new Date(a.time),
            keyB = new Date(b.time);
            // Compare the 2 dates
            if(keyA < keyB) return -1;
            if(keyA > keyB) return 1;
            return 0;
          });
          return ray;
        }

        var filter_minimum_rating = function(reviews){
          for (var i = reviews.length -1; i >= 0; i--) {
            if(reviews[i].rating < plugin.settings.min_rating){
              reviews.splice(i,1);
            }
          }
          return reviews;
        }

        var renderReviews = function(reviews){
          reviews = sort_by_date(reviews);
          reviews = filter_minimum_rating(reviews);
          var html = "";
          html = html
          + "<div class='s-swiper js__swiper-testimonials'>"
          + "<div class='swiper-wrapper g-margin-b-50--xs'>"

          var row_count = (plugin.settings.max_rows > 0)? plugin.settings.max_rows - 1 : reviews.length - 1;
          // make sure the row_count is not greater than available records
          row_count = (row_count > reviews.length-1)? reviews.length -1 : row_count;
          for (var i = row_count; i >= 0; i--) {
            var stars = renderStars(reviews[i].rating);
            var date = convertTime(reviews[i].time);
            var name = reviews[i].author_name + "</span><span class='review-sep'>, </span>";

            var format_before_line1 = "<div class='swiper-slide g-padding-x-130--sm g-padding-x-150--lg'>"
            var format_before_line2 = "<div class='g-padding-x-20--xs g-padding-x-50--lg'>"
            var format_before_line3 = "<div class='g-margin-b-40--xs'>"

            var format_after_line3 = "</div>"
            var format_after_line2 = "</div>"
            var format_after_line1 = "</div>"

            html = html
            + format_before_line1
            + format_before_line2
            + format_before_line3
            + "<div class='review-item'><div class='review-meta'><span class='review-author'>"
            + name 
            + "<span class='review-date'>" 
            + date 
            + "</span></div>" 
            + stars 
            + "<p class='review-text'>" 
            + reviews[i].text
            + "</p></div>"
            + format_after_line3
            + format_after_line2
            + format_after_line1
          };

          html = html
          + "</div>"
          + "<!-- Arrows -->"
          + "<div class='g-font-size-22--xs g-color--white-opacity js__swiper-fraction'></div>"
          + "<a href='javascript:void(0);' class='g-display-none--xs g-display-inline-block--sm s-swiper__arrow-v1--right s-icon s-icon--md s-icon--white-brd g-radius--circle ti-angle-right js__swiper-btn--next'></a>"
          + "<a href='javascript:void(0);' class='g-display-none--xs g-display-inline-block--sm s-swiper__arrow-v1--left s-icon s-icon--md s-icon--white-brd g-radius--circle ti-angle-left js__swiper-btn--prev'></a>"
          + "<!-- End Arrows -->"
          + "</div>"
          + "</div>"

          $element.append(html);
        }

        var renderStars = function(rating){
          var stars = "<div class='review-stars'><ul>";

          // fill in gold stars
          for (var i = 0; i < rating; i++) {
            stars = stars+"<li><i class='star'></i></li>";
          };

          // fill in empty stars
          if(rating < 5){
            for (var i = 0; i < (5 - rating); i++) {
              stars = stars+"<li><i class='star inactive'></i></li>";
            };
          }
          stars = stars+"</ul></div>";
          return stars;
        }

        var convertTime = function(UNIX_timestamp){
          var a = new Date(UNIX_timestamp * 1000);
          var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
          var time = months[a.getMonth()] + ' ' + a.getDate() + ', ' + a.getFullYear();
          return time;
        }

        plugin.init();

    }

    $.fn.googlePlaces = function(options) {

        return this.each(function() {
            if (undefined == $(this).data(namespace)) {
                var plugin = new $.googlePlaces(this, options);
                $(this).data(namespace, plugin);
            }
        });

    }

})(jQuery);
