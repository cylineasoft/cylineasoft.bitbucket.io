//---------------
//--- INORBIS --- SECONDARY
//---------------

var InOrbis = function () {

    return {
        init: function () {

        }
    }
}();

$(document).ready(function () {
    applyConstants();
    InOrbis.init();
});

//-----------------
//--- FULL PAGE ---
//-----------------

var myFullpage = new fullpage('#fullpage', {
    //--- LICENSE KEY
    licenseKey: 'D4B15E58-05D74A46-BA862218-F491DB7B',
    responsiveWidth: 420,

    //--- NAVIGATION
    menu: '#menu',
    lockAnchors: false,
    //anchors: ['firstPage', 'secondPage'],
    navigation: false,
    //navigationPosition: 'right',
    // navigationTooltips: ['firstSlide', 'secondSlide'],
    showActiveTooltip: false,
    slidesNavigation: false,
    slidesNavPosition: 'bottom',

    //--- SCROLLING
    css3: true,
    scrollingSpeed: 700,
    autoScrolling: false,
    fitToSection: true,
    fitToSectionDelay: 1000,
    scrollBar: false,
    easing: 'easeInOutCubic',
    easingcss3: 'ease',
    loopBottom: false,
    loopTop: false,
    loopHorizontal: true,
    continuousVertical: false,
    continuousHorizontal: false,
    scrollHorizontally: false,
    interlockedSlides: false,
    dragAndMove: false,
    offsetSections: false,
    resetSliders: false,
    fadingEffect: false,
    normalScrollElements: '#element1, .element2',
    scrollOverflow: true,
    scrollOverflowReset: false,
    scrollOverflowOptions: null,
    touchSensitivity: 15,
    bigSectionsDestination: null,

    //--- ACCESSIBILITY
    // keyboardScrolling: true,
    // animateAnchor: true,
    // recordHistory: true,

    //--- DESIGN
    // controlArrows: true,
    // verticalCentered: true,
    // sectionsColor: ['red', 'yellow', 'cyan', 'blue'],
    // paddingTop: '3em',
    // paddingBottom: '10px',
    // fixedElements: '#header, .footer',
    // responsiveWidth: 0,
    // responsiveHeight: 0,
    // responsiveSlides: false,
    // parallax: false,
    // parallaxOptions: {
    //     type: 'reveal',
    //     percentage: 62,
    //     property: 'translate'
    // },
    // cards: false,
    // cardsOptions: {
    //     perspective: 100,
    //     fadeContent: true,
    //     fadeBackground: true
    // },

    //--- CUSTOM SELECTORS
    sectionSelector: '.section',
    slideSelector: '.slide',

    lazyLoading: true,

    //--- EVENTS
    onLeave: function (origin, destination, direction) {
        if (destination.index == 0) {
            //--- show menus
            showMenus();
        } else {
            //--- hide menus
            hideMenus();
        }
    },
    afterLoad: function (origin, destination, direction) {},
    afterRender: function () {},
    afterResize: function (width, height) {},
    afterReBuild: function () {},
    afterResponsive: function (isResponsive) {},
    afterSlideLoad: function (section, origin, destination, direction) {},
    onSlideLeave: function (section, origin, destination, direction) {}
});

// var myFullpage = new fullpage('#fullpage', {
//     //--- LICENSE KEY
//     licenseKey: 'D4B15E58-05D74A46-BA862218-F491DB7B',
//     responsiveWidth: 420,

//     //--- NAVIGATION
//     menu: '#menu',
//     //    anchors: ['firstPage', 'secondPage', '3rdPage'],
//     autoScrolling: false,

//     lazyLoading: true,

//     //--- EVENTS
//     onLeave: function (origin, destination, direction) {
//         if (destination.index == 0) {
//             //--- show menus
//             showMenus();
//         } else {
//             //--- hide menus
//             hideMenus();
//         }
//     },
//     afterLoad: function (origin, destination, direction) {},
//     afterRender: function () {},
//     afterResize: function (width, height) {},
//     afterReBuild: function () {},
//     afterResponsive: function (isResponsive) {},
//     afterSlideLoad: function (section, origin, destination, direction) {},
//     onSlideLeave: function (section, origin, destination, direction) {}
// });

function showMenus() {
    var menus = document.getElementsByClassName("header-menu");
    for (var i = 0; i < menus.length; i++) {
        jQuery(menus.item(i)).fadeIn(2000); //-- require jQuery around element or error: 'fadeIn is not a function'
    }
}

function hideMenus() {
    var menus = document.getElementsByClassName("header-menu");
    for (var i = 0; i < menus.length; i++) {
        jQuery(menus.item(i)).fadeOut(); //-- require jQuery around element or error: 'fadeOut is not a function'
    }
}

//-----------------
//--- CONSTANTS ---
//-----------------

function applyConstants() {
    const inorbis_logo_default = "img/logo-green.png";
    const inorbis_logo_shrink = "img/inorbis_logo/II LOGO ORB WHITE - GREEN.png";
    const inorbis_logo_alt = "InOrbis";

    const inorbis_contact_phone = '+1 877 601 8747';
    const inorbis_contact_email = 'info@inorbis.ca';
    const inorbis_contact_email_subject = '?Subject=InOrbis%20contact%20request'

    //--- HEADER - InOrbis LOGO - default
    var logos1 = document.getElementsByClassName("s-header__logo-img-default");
    for (var i = 0; i < logos1.length; i++) {
        logos1.item(i).src = inorbis_logo_default;
        logos1.item(i).alt = inorbis_logo_alt;
    }

    //--- HEADER - InOrbis LOGO - shrink
    var logos2 = document.getElementsByClassName("s-header__logo-img-shrink");
    for (var i = 0; i < logos2.length; i++) {
        logos2.item(i).src = inorbis_logo_shrink;
        logos2.item(i).alt = inorbis_logo_alt;
    }

    //--- FOOTER - InOrbis LOGO - shrink
    var logos3 = document.getElementsByClassName("s-footer__logo-img");
    for (var i = 0; i < logos3.length; i++) {
        logos3.item(i).src = inorbis_logo_shrink;
        logos3.item(i).alt = inorbis_logo_alt;
    }

    //--- InOrbis PHONE NUMBER
    var phones = document.getElementsByClassName("inorbis-phone");
    for (var i = 0; i < phones.length; i++) {
        phones.item(i).href = "tel:" + inorbis_contact_phone;
        phones.item(i).innerHTML = inorbis_contact_phone;
    }

    //--- InOrbis EMAIL
    var emailes = document.getElementsByClassName("inorbis-email");
    for (var i = 0; i < emailes.length; i++) {
        var aEmail = emailes.item(i);
        emailes.item(i).href = "mailto:" + inorbis_contact_email + inorbis_contact_email_subject;
        emailes.item(i).innerHTML = inorbis_contact_email;
    }
}

$(document).ready(function () {
    $('.modal-btn').magnificPopup({
        type: 'inline',
        closeBtnInside: true,
        autoFocusLast: true,
        focus: ".modal-title",
    });
});