//---------------
//--- INORBIS ---
//---------------

var InOrbis = function () {

    return {
        init: function () {

            $("#google-reviews").googlePlaces({
                placeId: 'ChIJwWhzmxFpcVMRSq8hNNO34Cg',
                render: ['reviews'],
                min_rating: 4,
                max_rows: 5
            });

        }
    }
}();

$(document).ready(function () {
    //   inorbisHeaderMain();
    applyConstants();
    InOrbis.init();
});



//-----------------
//--- FULL PAGE ---
//-----------------


var myFullpage = new fullpage('#fullpage', {
    //--- LICENSE KEY
    licenseKey: 'D4B15E58-05D74A46-BA862218-F491DB7B',
    responsiveWidth: 420,

    //--- NAVIGATION
    menu: '#menu',
    lockAnchors: false,
    anchors: ['panel1', 'panel2', 'panel3', 'panel4'],
    navigation: true,
    navigationPosition: 'right',
    // navigationTooltips: ['firstSlide', 'secondSlide'],
    showActiveTooltip: false,
    slidesNavigation: false,
    slidesNavPosition: 'bottom',

    //--- SCROLLING
    css3: true,
    scrollingSpeed: 700,
    autoScrolling: true,
    fitToSection: true,
    fitToSectionDelay: 1000,
    scrollBar: false,
    easing: 'easeInOutCubic',
    easingcss3: 'ease',
    loopBottom: false,
    loopTop: false,
    loopHorizontal: true,
    continuousVertical: false,
    continuousHorizontal: false,
    scrollHorizontally: false,
    interlockedSlides: false,
    dragAndMove: false,
    offsetSections: false,
    resetSliders: false,
    fadingEffect: false,
    normalScrollElements: '#element1, .element2',
    scrollOverflow: false,
    scrollOverflowReset: false,
    scrollOverflowOptions: null,
    touchSensitivity: 15,
    bigSectionsDestination: null,

    //--- ACCESSIBILITY
    // keyboardScrolling: true,
    // animateAnchor: true,
    // recordHistory: true,

    //--- DESIGN
    // controlArrows: true,
    // verticalCentered: true,
    // sectionsColor: ['#ccc', '#fff'],
    // paddingTop: '3em',
    // paddingBottom: '10px',
    // fixedElements: '#header, .footer',
    // responsiveWidth: 0,
    // responsiveHeight: 0,
    // responsiveSlides: false,
    // parallax: false,
    // parallaxOptions: {
    //     type: 'reveal',
    //     percentage: 62,
    //     property: 'translate'
    // },
    // cards: false,
    // cardsOptions: {
    //     perspective: 100,
    //     fadeContent: true,
    //     fadeBackground: true
    // },

    //--- CUSTOM SELECTORS
    sectionSelector: '.section',
    slideSelector: '.slide',

    lazyLoading: true,

    //--- EVENTS
    onLeave: function (origin, destination, direction) {
        if ( destination.index == 0 ) {
            //--- show menus
            showMenus();
        } else {
            //--- hide menus
            hideMenus();
        }
    },
    afterLoad: function (origin, destination, direction) {},
    afterRender: function () {},
    afterResize: function (width, height) {},
    afterReBuild: function () {},
    afterResponsive: function (isResponsive) {},
    afterSlideLoad: function (section, origin, destination, direction) {},
    onSlideLeave: function (section, origin, destination, direction) {}
});

function showMenus() {
    var menus = document.getElementsByClassName("header-menu");
    for (var i = 0; i < menus.length; i++) {
        jQuery(menus.item(i)).fadeIn(2000); //-- require jQuery around element or error: 'fadeIn is not a function'
    }
}
function hideMenus() {
    var menus = document.getElementsByClassName("header-menu");
    for (var i = 0; i < menus.length; i++) {
        jQuery(menus.item(i)).fadeOut(); //-- require jQuery around element or error: 'fadeOut is not a function'
    }
}

//-----------------
//--- CONSTANTS ---
//-----------------

function applyConstants() {
    const inorbis_logo_default = "img/logo-white.png";
    const inorbis_logo_shrink = "img/logo-shrink.png";
    const inorbis_logo_alt = "InOrbis";

    const inorbis_contact_phone = '+1 877 601 8747';
    const inorbis_contact_email = 'info@inorbis.ca';
    const inorbis_contact_email_subject = '?Subject=InOrbis%20contact%20request'

    //--- HEADER - InOrbis LOGO - default
    var logos1 = document.getElementsByClassName("s-header__logo-img-default");
    for (var i = 0; i < logos1.length; i++) {
        logos1.item(i).src = inorbis_logo_default;
        logos1.item(i).alt = inorbis_logo_alt;
    }

    //--- HEADER - InOrbis LOGO - shrink
    var logos2 = document.getElementsByClassName("s-header__logo-img-shrink");
    for (var i = 0; i < logos2.length; i++) {
        logos2.item(i).src = inorbis_logo_shrink;
        logos2.item(i).alt = inorbis_logo_alt;
    }

    //--- FOOTER - InOrbis LOGO - shrink
    var logos3 = document.getElementsByClassName("s-footer__logo-img");
    for (var i = 0; i < logos3.length; i++) {
        logos3.item(i).src = inorbis_logo_shrink;
        logos3.item(i).alt = inorbis_logo_alt;
    }

    //--- InOrbis PHONE NUMBER
    var phones = document.getElementsByClassName("inorbis-phone");
    for (var i = 0; i < phones.length; i++) {
        phones.item(i).href = "tel:" + inorbis_contact_phone;
        phones.item(i).innerHTML = inorbis_contact_phone;
    }

    //--- InOrbis EMAIL
    var emailes = document.getElementsByClassName("inorbis-email");
    for (var i = 0; i < emailes.length; i++) {
        var aEmail = emailes.item(i);
        emailes.item(i).href = "mailto:" + inorbis_contact_email + inorbis_contact_email_subject;
        emailes.item(i).innerHTML = inorbis_contact_email;
    }

}

function inorbisHeaderMain() {
    var strVar = "";
    strVar += "    <header class=\"navbar-fixed-top s-header js__header-sticky js__header-overlay\">";
    strVar += "        <!-- Navbar -->";
    strVar += "        <div class=\"s-header__navbar\">";
    strVar += "            <div class=\"s-header__container\">";
    strVar += "                <div class=\"s-header__navbar-row\">";
    strVar += "                    <div class=\"s-header__navbar-row-col\">";
    strVar += "                        <div class=\"s-header__logo\">";
    strVar += "                            <!-- Logo -->";
    strVar += "                            <a href=\"index.html\" class=\"s-header__logo-link\">";
    strVar += "                                <img class=\"s-header__logo-img s-header__logo-img-default\" src=\"aaa\" alt=\"bbb\">";
    strVar += "                                <img class=\"s-header__logo-img s-header__logo-img-shrink\" src=\"ccc\" alt=\"ddd\">";
    strVar += "                            <\/a>";
    strVar += "                            <!-- End Logo -->";
    strVar += "                        <\/div>";
    strVar += "                    <\/div>";
    strVar += "                    <div class=\"s-header__navbar-row-col header-menu\">";
    strVar += "                        <div class=\"g-text-right--xs\">";
    strVar += "                            <ul class=\"list-inline\">";
    strVar += "                                <li class=\"\"><a class=\"\" href=\"index_app_landing.html\">About<\/a><\/li>";
    strVar += "                                <li class=\"\"><a class=\"\" href=\"index_portfolsio.html\">Services<\/a><\/li>";
    strVar += "                                <li class=\"\"><a class=\"\" href=\"index_events.html\">Cars<\/a><\/li>";
    strVar += "                                <li class=\"\"><a class=\"\" href=\"index_events.html\">Media<\/a><\/li>";
    strVar += "                                <li class=\"\"><a class=\"\" href=\"index_events.html\">Contact<\/a><\/li>";
    strVar += "                                <li class=\"\"><a class=\"\" href=\"index_events.html\">Sign In<\/a><\/li>";
    strVar += "                            <\/ul>";
    strVar += "                        <\/div>";
    strVar += "                    <\/div>";
    strVar += "                    <div class=\"s-header__navbar-row-col header-menu\">";
    strVar += "                        <!-- Trigger -->";
    strVar += "                        <a href=\"javascript:void(0);\" class=\"s-header__trigger js__trigger\">";
    strVar += "                            <span class=\"s-header__trigger-icon\"><\/span>";
    strVar += "                            <svg x=\"0rem\" y=\"0rem\" width=\"3.125rem\" height=\"3.125rem\" viewbox=\"0 0 54 54\">";
    strVar += "                                <circle fill=\"transparent\" stroke=\"#fff\" stroke-width=\"1\" cx=\"27\" cy=\"27\" r=\"25\"";
    strVar += "                                    stroke-dasharray=\"157 157\" stroke-dashoffset=\"157\"><\/circle>";
    strVar += "                            <\/svg>";
    strVar += "                        <\/a>";
    strVar += "                        <!-- End Trigger -->";
    strVar += "                    <\/div>";
    strVar += "                <\/div>";
    strVar += "            <\/div>";
    strVar += "        <\/div>";
    strVar += "        <!-- End Navbar -->";
    strVar += "";
    strVar += "        <!-- Overlay -->";
    strVar += "        <div class=\"s-header-bg-overlay js__bg-overlay\">";
    strVar += "            <!-- Nav -->";
    strVar += "            <nav class=\"s-header__nav js__scrollbar\">";
    strVar += "                <div class=\"container-fluid\">";
    strVar += "                    <!-- Menu List -->";
    strVar += "                    <ul class=\"list-unstyled s-header__nav-menu\">";
    strVar += "                        <li class=\"s-header__nav-menu-item\"><a";
    strVar += "                                class=\"s-header__nav-menu-link s-header__nav-menu-link-divider -is-active\"";
    strVar += "                                href=\"index.html\">Corporate<\/a><\/li>";
    strVar += "                        <li class=\"s-header__nav-menu-item\"><a";
    strVar += "                                class=\"s-header__nav-menu-link s-header__nav-menu-link-divider\"";
    strVar += "                                href=\"index_app_landing.html\">App Landing<\/a><\/li>";
    strVar += "                        <li class=\"s-header__nav-menu-item\"><a";
    strVar += "                                class=\"s-header__nav-menu-link s-header__nav-menu-link-divider\"";
    strVar += "                                href=\"index_portfolio.html\">Portfolio<\/a><\/li>";
    strVar += "                        <li class=\"s-header__nav-menu-item\"><a";
    strVar += "                                class=\"s-header__nav-menu-link s-header__nav-menu-link-divider\"";
    strVar += "                                href=\"index_events.html\">Events<\/a><\/li>";
    strVar += "                        <li class=\"s-header__nav-menu-item\"><a";
    strVar += "                                class=\"s-header__nav-menu-link s-header__nav-menu-link-divider\"";
    strVar += "                                href=\"index_lawyer.html\">Lawyer<\/a><\/li>";
    strVar += "                        <li class=\"s-header__nav-menu-item\"><a";
    strVar += "                                class=\"s-header__nav-menu-link s-header__nav-menu-link-divider\"";
    strVar += "                                href=\"index_clinic.html\">Clinic<\/a><\/li>";
    strVar += "                        <li class=\"s-header__nav-menu-item\"><a";
    strVar += "                                class=\"s-header__nav-menu-link s-header__nav-menu-link-divider\"";
    strVar += "                                href=\"index_coming_soon.html\">Coming Soon<\/a><\/li>";
    strVar += "                    <\/ul>";
    strVar += "                    <!-- End Menu List -->";
    strVar += "";
    strVar += "                    <!-- Menu List -->";
    strVar += "                    <ul class=\"list-unstyled s-header__nav-menu\">";
    strVar += "                        <li class=\"s-header__nav-menu-item\"><a";
    strVar += "                                class=\"s-header__nav-menu-link s-header__nav-menu-link-divider\"";
    strVar += "                                href=\"about.html\">About<\/a>";
    strVar += "                        <\/li>";
    strVar += "                        <li class=\"s-header__nav-menu-item\"><a";
    strVar += "                                class=\"s-header__nav-menu-link s-header__nav-menu-link-divider\"";
    strVar += "                                href=\"team.html\">Team<\/a>";
    strVar += "                        <\/li>";
    strVar += "                        <li class=\"s-header__nav-menu-item\"><a";
    strVar += "                                class=\"s-header__nav-menu-link s-header__nav-menu-link-divider\"";
    strVar += "                                href=\"services.html\">Services<\/a><\/li>";
    strVar += "                        <li class=\"s-header__nav-menu-item\"><a";
    strVar += "                                class=\"s-header__nav-menu-link s-header__nav-menu-link-divider\"";
    strVar += "                                href=\"events.html\">Events<\/a><\/li>";
    strVar += "                        <li class=\"s-header__nav-menu-item\"><a";
    strVar += "                                class=\"s-header__nav-menu-link s-header__nav-menu-link-divider\" href=\"faq.html\">FAQ<\/a>";
    strVar += "                        <\/li>";
    strVar += "                        <li class=\"s-header__nav-menu-item\"><a";
    strVar += "                                class=\"s-header__nav-menu-link s-header__nav-menu-link-divider\"";
    strVar += "                                href=\"contacts.html\">Contacts<\/a><\/li>";
    strVar += "                    <\/ul>";
    strVar += "                    <!-- End Menu List -->";
    strVar += "                <\/div>";
    strVar += "            <\/nav>";
    strVar += "            <!-- End Nav -->";
    strVar += "";
    strVar += "            <!-- Action -->";
    strVar += "            <!-- <ul class=\"list-inline s-header__action s-header__action--lb\">";
    strVar += "                <li class=\"s-header__action-item\"><a class=\"s-header__action-link -is-active\" href=\"#\">En<\/a><\/li>";
    strVar += "                <li class=\"s-header__action-item\"><a class=\"s-header__action-link\" href=\"#\">Fr<\/a><\/li>";
    strVar += "            <\/ul> -->";
    strVar += "            <!-- End Action -->";
    strVar += "";
    strVar += "            <!-- Action -->";
    strVar += "            <ul class=\"list-inline s-header__action s-header__action--rb\">";
    strVar += "                <li class=\"s-header__action-item\">";
    strVar += "                    <a class=\"s-header__action-link\" href=\"#\">";
    strVar += "                        <i class=\"g-padding-r-5--xs ti-facebook\"><\/i>";
    strVar += "                        <span class=\"g-display-none--xs g-display-inline-block--sm\">Facebook<\/span>";
    strVar += "                    <\/a>";
    strVar += "                <\/li>";
    strVar += "                <li class=\"s-header__action-item\">";
    strVar += "                    <a class=\"s-header__action-link\" href=\"#\">";
    strVar += "                        <i class=\"g-padding-r-5--xs ti-twitter\"><\/i>";
    strVar += "                        <span class=\"g-display-none--xs g-display-inline-block--sm\">Twitter<\/span>";
    strVar += "                    <\/a>";
    strVar += "                <\/li>";
    strVar += "                <li class=\"s-header__action-item\">";
    strVar += "                    <a class=\"s-header__action-link\" href=\"#\">";
    strVar += "                        <i class=\"g-padding-r-5--xs ti-instagram\"><\/i>";
    strVar += "                        <span class=\"g-display-none--xs g-display-inline-block--sm\">Instagram<\/span>";
    strVar += "                    <\/a>";
    strVar += "                <\/li>";
    strVar += "            <\/ul>";
    strVar += "            <!-- End Action -->";
    strVar += "        <\/div>";
    strVar += "        <!-- End Overlay -->";
    strVar += "    <\/header>";
    strVar += "";

    console.log('strVar' + strVar);
    var d1 = document.getElementById('aaaaaa');
    d1.insertAdjacentHTML('afterend', '<div id="two">HELLO HELLO</div>');
    // var d2 = document.getElementById('bbbbbb');
    // d2.insertAdjacentHTML('afterend', strVar);
    console.log('strVar' + "HELLO WORLD" + d1);
}